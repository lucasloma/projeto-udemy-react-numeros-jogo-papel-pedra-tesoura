import React from 'react';
import { StyleSheet, View,Button,ScrollView} from 'react-native';
import TOPO from './components/topo';
import ICON from './components/icon';


export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            escolhaUsuario: '',
            escolhaComputador: '',
            resultado: '',
            ELEMENTOS: ['Pedra', 'Tesoura', 'Papel'],
        }
    };

    jokenpo(valor) {
        const valorPC = this.state.ELEMENTOS[escolhaPC()];
        const resultado = calculaResultado(valor, valorPC);
        this.setState ({
                escolhaUsuario: valor,
                escolhaComputador: valorPC,
                resultado: resultado
            }
        );
    };

  render() {
    return (
      <ScrollView>
          <TOPO/>
          <View style={styles.painelAcao}>
              <View style={styles.btnEscolha} >
                  <Button title='Pedra' onPress={() => {this.jokenpo(this.state.ELEMENTOS[0])}}/>
              </View>

              <View style={styles.btnEscolha} >
                  <Button title='Tesoura' onPress={() => {this.jokenpo(this.state.ELEMENTOS[1])}}/>
              </View>

              <View style={styles.btnEscolha} >
                  <Button title='Papel' onPress={() => {this.jokenpo(this.state.ELEMENTOS[2])}}/>
              </View>
          </View>
          <View style={styles.painelResultado}>
              <ICON escolha={this.state.escolhaUsuario} jogador='Você' resultado={this.state.resultado }/>
              <ICON escolha={this.state.escolhaComputador} jogador='Computador'/>
          </View>
      </ScrollView>
    );
  };
};

function escolhaPC() {
    let numeroAleatorio =  Math.random();
    numeroAleatorio = Math.floor(numeroAleatorio * 3);
    return numeroAleatorio;
}

function calculaResultado(escolhaUsuario,escolhaComputador){
    let resultado = '';

    if(escolhaUsuario === escolhaComputador){
        resultado = 'Empantou';
    }
    if(escolhaUsuario === 'Pedra' && escolhaComputador === 'Papel'){
        resultado = 'Perdeu!!!!!';
    }
    if(escolhaUsuario === 'Papel' && escolhaComputador === 'Pedra'){
        resultado = 'Ganhou!!!!!';
    }
    if(escolhaUsuario === 'Pedra' && escolhaComputador === 'Tesoura'){
        resultado = 'Ganhou!!!!!';
    }
    if(escolhaUsuario === 'Tesoura' && escolhaComputador === 'Pedra'){
        resultado = 'Perdeu!!!!!';
    }
    if(escolhaUsuario === 'Tesoura' && escolhaComputador === 'Papel'){
        resultado = 'Ganhou!!!!!';
    }
    if(escolhaUsuario === 'Papel' && escolhaComputador === 'Tesoura'){
        resultado = 'Perdeu!!!!!';
    }
    return resultado;
}

const styles = StyleSheet.create({
    painelAcao:{
        flexDirection:'row',
        marginTop:20,
        justifyContent:'space-between'
    },
    btnEscolha:{
        width: 90
    },
    painelResultado:{
      alignItems:'center',
        marginTop:20
    },
});
