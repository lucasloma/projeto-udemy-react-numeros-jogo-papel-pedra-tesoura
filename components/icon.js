import React from 'react';
import { StyleSheet, Text, View,Image } from 'react-native';

const IMAGEMPEDRA = require('../assets/pedra.png');
const IMAGEMTESOURA = require('../assets/tesoura.png');
const IMAGEMPAPEL = require('../assets/papel.png');

class ICON extends React.Component {
    render() {
        const { escolha,jogador,resultado }= this.props;
        if(escolha === 'Pedra'){
            return (
                <View style={styles.container}>
                    <Text style={styles.textResultado}>{jogador} {resultado}</Text>
                    <Image source={IMAGEMPEDRA}/>
                </View>
            );
        }
        if(escolha === 'Tesoura'){
            return (
                <View style={styles.container}>
                    <Text style={styles.textResultado}>{jogador} {resultado}</Text>
                    <Image source={IMAGEMTESOURA}/>
                </View>
            );
        }
        if(escolha === 'Papel') {
            return (
                <View style={styles.container}>
                    <Text style={styles.textResultado} >{jogador} {resultado}</Text>
                    <Image source={IMAGEMPAPEL}/>
                </View>
            );
        }
        return null
    };
}

const styles = StyleSheet.create({
    textResultado: {
        fontSize: 25,
        fontWeight: 'bold',
        color: 'red',
    },
    container:{
        alignItems:'center',
        marginBottom:20
    }
});

export default ICON;