import React from 'react';
import { StyleSheet,ImageBackground } from 'react-native';

const IMAGE = require('../assets/logo.jpg');

const TOPO = () => {
    return (
        <ImageBackground style={styles.imagem}
                source={IMAGE}>
        </ImageBackground>
    )
};

const styles = StyleSheet.create({
    img2: {
        flex: 1,
    },
    imagem: {
        flex: 1,
        aspectRatio: 2,
        //resizeMode:"cover",
        width: '100%',
        height: '100%',
        marginTop:25,
    },
});

export default TOPO;